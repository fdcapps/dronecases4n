/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fdcapps.dronecases4n.objects;

/**
 * DroneImpl is the default implementation of the Drone interface
 *
 * @author Felipe Diaz C <felipediazc@fdcapps.com>
 */
public class DroneImpl implements Drone {

    Integer x;
    Integer y;
    Orientation o;

    public DroneImpl() {
        this.x = null;
        this.y = null;
        this.o = null;
    }
    
    public DroneImpl(Integer x, Integer y, Orientation o) {
        this.x = x;
        this.y = y;
        this.o = o;
    }    
    /**
     * This method is used to get the X coordinate.
     *
     * @return int This returns the value of the coordinate in the X axis.
     */
    @Override
    public Integer getX() {
        return x;
    }

    /**
     * This method is used to get the Y coordinate.
     *
     * @return int This returns the value of the coordinate in the Y axis.
     */
    @Override
    public Integer getY() {
        return y;
    }

    /**
     * This method is used to get the Drone orientation (where is it pointing?).
     *
     * @return int This returns the current Drone orientation.
     */
    @Override
    public Orientation getOrientation() {
        return o;
    }

    /**
     * This method is used to set the X coordinate.
     *
     * @param x This is the value of the X coordinate
     */
    @Override
    public void setX(Integer x) {
        this.x = x;
    }

    /**
     * This method is used to set the Y coordinate.
     *
     * @param y This is the value of the Y coordinate
     */
    @Override
    public void setY(Integer y) {
        this.y = y;
    }

    /**
     * This method is used to set the Drone orientation.
     *
     * @param o This is the value of the drone orientation
     */
    @Override
    public void setOrientation(Orientation o) {
        this.o = o;
    }

}
