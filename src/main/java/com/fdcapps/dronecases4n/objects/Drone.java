/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fdcapps.dronecases4n.objects;

/**
 * Drone is an interface which acts as a specification of a drone object
 *
 * @author Felipe Diaz C <felipediazc@fdcapps.com>
 */
public interface Drone {

    /**
     * This method is used to get the X coordinate.
     *
     * @return int This returns the value of the coordinate in the X axis.
     */
    public Integer getX();

    /**
     * This method is used to get the Y coordinate.
     *
     * @return int This returns the value of the coordinate in the Y axis.
     */
    public Integer getY();

    /**
     * This method is used to get the Drone orientation (where is it pointing?).
     *
     * @return int This returns the current Drone orientation.
     */
    public Orientation getOrientation();

    /**
     * This method is used to set the X coordinate.
     *
     * @param x This is the value of the X coordinate
     */
    public void setX(Integer x);

    /**
     * This method is used to set the Y coordinate.
     *
     * @param y This is the value of the Y coordinate
     */
    public void setY(Integer y);

    /**
     * This method is used to set the Drone orientation.
     *
     * @param o This is the value of the drone orientation
     */
    public void setOrientation(Orientation o);

}
