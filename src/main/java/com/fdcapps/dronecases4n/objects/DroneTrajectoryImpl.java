/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fdcapps.dronecases4n.objects;

/**
 * DroneTrajectoryImpl is the default implementation of the DroneTrajectory
 * interface
 *
 * @author Felipe Diaz C <felipediazc@fdcapps.com>
 */
public class DroneTrajectoryImpl implements DroneTrajectory {

    String trajectory;

    public DroneTrajectoryImpl(String t) {
        this.trajectory = t;
    }

    /**
     * This method is used to get the Drone trajectory.
     *
     * @return String This is a string representation of a Drone trajectory
     * where: : A -> is a move forward. D -> is 90 degree right turn from drone
     * orientation. I -> is 90 degree right turn from drone orientation
     */
    @Override
    public String getTrajectory() {
        return trajectory;
    }

    /**
     * This method is used to set the Drone trajectory.
     *
     * @param s This is the value of the trajecory which is a string like
     * AAAAIDA where: A -> is a move forward. D -> is 90 degree right turn from
     * drone orientation. I -> is 90 degree right turn from drone orientation
     */
    @Override
    public void setTrajectory(String s) {
        this.trajectory = s;
    }

}
