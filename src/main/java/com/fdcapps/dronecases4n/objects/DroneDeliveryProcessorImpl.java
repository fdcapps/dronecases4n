/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fdcapps.dronecases4n.objects;

import java.util.ArrayList;
import java.util.List;

/**
 * DroneDeliveryProcessorImpl is the default implementation of the
 * DroneDeliveryProcessor interface
 *
 * @author Felipe Diaz C <felipediazc@fdcapps.com>
 */
public class DroneDeliveryProcessorImpl implements DroneDeliveryProcessor {

    /**
     * This method is used to get the Drone delivery data based on a drone
     * trajectory.
     *
     * @param drone This is the Drone object in which its coordinates and
     * orientation will be calculated
     * @param t This is the Drone trajectory
     * @return a Drone object with its coordinates and orientation
     * @throws java.lang.Exception
     */
    @Override
    public Drone getDeliveryStatus(Drone drone, DroneTrajectory t) throws Exception {
        if (t == null) {
            throw new Exception("Error. Drone trajectory is null");
        }
        String s = t.getTrajectory();
        for (int i = 0; i < s.length(); i++) {
            char indication = s.charAt(i);
            drone = updateLocation(drone, indication);
        }
        return drone;
    }

    public Drone updateLocation(Drone d, char indication) {
        if (d.getOrientation() == Orientation.NORTH && indication == 'I') {
            d.setOrientation(Orientation.WEST);
        } else if (d.getOrientation() == Orientation.NORTH && indication == 'D') {
            d.setOrientation(Orientation.EAST);
        } else if (d.getOrientation() == Orientation.SOUTH && indication == 'I') {
            d.setOrientation(Orientation.EAST);
        } else if (d.getOrientation() == Orientation.SOUTH && indication == 'D') {
            d.setOrientation(Orientation.WEST);
        } else if (d.getOrientation() == Orientation.EAST && indication == 'I') {
            d.setOrientation(Orientation.NORTH);
        } else if (d.getOrientation() == Orientation.EAST && indication == 'D') {
            d.setOrientation(Orientation.SOUTH);
        } else if (d.getOrientation() == Orientation.WEST && indication == 'I') {
            d.setOrientation(Orientation.SOUTH);
        } else if (d.getOrientation() == Orientation.WEST && indication == 'D') {
            d.setOrientation(Orientation.NORTH);
        } else if (indication == 'A') {
            if (null != d.getOrientation()) {
                switch (d.getOrientation()) {
                    case SOUTH:
                        d.setY(d.getY() - 1);
                        break;
                    case NORTH:
                        d.setY(d.getY() + 1);
                        break;
                    case WEST:
                        d.setX(d.getX() - 1);
                        break;
                    case EAST:
                        d.setX(d.getX() + 1);
                        break;
                    default:
                        break;
                }
            }
        }
        return d;
    }

    @Override
    public List<Drone> getDeliveryStatus(List<DroneTrajectory> trajectoryList) throws Exception {
        List<Drone> list = new ArrayList<>();
        Drone drone = new DroneImpl(0, 0, Orientation.NORTH);
        for (int i = 0; i < trajectoryList.size(); i++) {
            DroneTrajectory t = trajectoryList.get(i);
            drone = getDeliveryStatus(drone, t);
            list.add(new DroneImpl(drone.getX(), drone.getY(), drone.getOrientation()));
        }
        return list;
    }

}
