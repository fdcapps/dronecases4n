/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fdcapps.dronecases4n.objects;

import java.util.List;

/**
 * DroneDeliveryProcessor is an interface which acts as a specification of a
 * object which calculates a drone trajectory based on a delivery object
 *
 * @author Felipe Diaz C <felipediazc@fdcapps.com>
 */
public interface DroneDeliveryProcessor {

    /**
     * This method is used to get the Drone delivery data based on a drone
     * trajectory.
     *
     * @param d This is the Drone object in which its coordinates and
     * orientation will be calculated
     * @param t This is the Drone trajectory
     * @return a Drone object with its coordinates and orientation
     * @throws java.lang.Exception
     */
    public Drone getDeliveryStatus(Drone d, DroneTrajectory t) throws Exception;

    /**
     * This method is used to get the Drone delivery data based on a drone
     * trajectory.
     *
     * @param trajectoryList is the list of drone trajectories
     * @return a List which its elements are Drone objects with its coordinates
     * and orientation
     * @throws java.lang.Exception
     */
    public List<Drone> getDeliveryStatus(List<DroneTrajectory> trajectoryList) throws Exception;

}
