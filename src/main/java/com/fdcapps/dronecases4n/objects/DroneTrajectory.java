/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fdcapps.dronecases4n.objects;

/**
 * DroneTrajectory is an interface which acts as a specification of a drone
 * trajectory object
 *
 * @author Felipe Diaz C <felipediazc@fdcapps.com>
 */
public interface DroneTrajectory {

    /**
     * This method is used to get the Drone trajectory.
     *
     * @return String This is a string representation of a Drone trajectory
     * where: : A -> is a move forward. D -> is 90 degree right turn from drone
     * orientation. I -> is 90 degree right turn from drone orientation
     */
    public String getTrajectory();

    /**
     * This method is used to set the Drone trajectory.
     *
     * @param s This is the value of the trajecory which is a string like
     * AAAAIDA where: A -> is a move forward. D -> is 90 degree right turn from
     * drone orientation. I -> is 90 degree right turn from drone orientation
     */
    public void setTrajectory(String s);

}
