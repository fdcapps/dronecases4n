/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fdcapps.dronecases4n.objects;

/**
 * This Enumeration represents the orientation in the cardinal points
 * @author Felipe Diaz C <felipediazc@fdcapps.com>
 */
public enum Orientation {
    NORTH, SOUTH, EAST, WEST
}
