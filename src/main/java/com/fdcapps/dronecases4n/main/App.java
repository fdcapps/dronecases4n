/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fdcapps.dronecases4n.main;

import com.fdcapps.dronecases4n.input.DroneDeliveryInputFileImpl;
import com.fdcapps.dronecases4n.objects.Drone;
import com.fdcapps.dronecases4n.objects.DroneDeliveryProcessorImpl;
import com.fdcapps.dronecases4n.objects.DroneTrajectory;
import com.fdcapps.dronecases4n.output.DroneDeliveryOutputFileImpl;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * This is the project's main file
 *
 * @author Felipe Diaz C <felipediazc@fdcapps.com>
 */
public class App {

    private static final Logger log = Logger.getLogger(App.class.getName());

    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Invalid command line, we are expecting for a file path (ex. textFiles/example/in01.txt");
            System.exit(1);
        }
        File inputFile = new File(args[0]);
        String path = inputFile.getPath();
        String fileName = inputFile.getName();
        path = path.replaceAll(fileName, "");
        try {
            InputStream inputStream = new FileInputStream(inputFile);
            DroneDeliveryInputFileImpl deliveryInputFile = new DroneDeliveryInputFileImpl();
            List<DroneTrajectory> trajectoryList = deliveryInputFile.getDeliveryInput(inputStream);

            DroneDeliveryProcessorImpl deliverProcessor = new DroneDeliveryProcessorImpl();
            List<Drone> droneStatusList = deliverProcessor.getDeliveryStatus(trajectoryList);

            DroneDeliveryOutputFileImpl output = new DroneDeliveryOutputFileImpl();
            String outputFile = fileName.replace("in", "out");
            FileOutputStream out = new FileOutputStream(path + "/" + outputFile);
            output.getDroneDelivery(out, droneStatusList);
            out.close();
        } catch (Exception e) {
            log.error(e);
        }

    }
}
