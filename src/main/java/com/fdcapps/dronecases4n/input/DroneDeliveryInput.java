/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fdcapps.dronecases4n.input;

import com.fdcapps.dronecases4n.objects.DroneTrajectory;
import java.io.InputStream;
import java.util.List;

/**
 * DroneDeliveryInput is an interface which represents an input of a drone
 * trajectory
 *
 * @author Felipe Diaz C <felipediazc@fdcapps.com>
 */
public interface DroneDeliveryInput {

    /**
     * This method is used as a drone trajectory input.
     *
     * @param in This is the InputStream where de data comes as an input
     * @return a List of drone trajectories
     * @throws java.lang.Exception
     */
    List<DroneTrajectory> getDeliveryInput(InputStream in) throws Exception;
}
