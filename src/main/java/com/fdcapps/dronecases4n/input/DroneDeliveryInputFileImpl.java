/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fdcapps.dronecases4n.input;

import com.fdcapps.dronecases4n.objects.DroneTrajectory;
import com.fdcapps.dronecases4n.objects.DroneTrajectoryImpl;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * DroneDeliveryInputImpl is the default implementation of the
 * DroneDeliveryInput interface
 *
 * @author Felipe Diaz C <felipediazc@fdcapps.com>
 */
public class DroneDeliveryInputFileImpl implements DroneDeliveryInput {

    private static final Logger log = Logger.getLogger(DroneDeliveryInputFileImpl.class.getName());

    /**
     * This method is used as a drone trajectory input.
     *
     * @param in This is the InputStream where de data comes as an input
     * @return a List of drone trajectories
     * @throws java.lang.Exception
     */
    @Override
    public List<DroneTrajectory> getDeliveryInput(InputStream in) throws Exception {
        String text;
        List<DroneTrajectory> droneTrajectoryList = new ArrayList<>();
        try {
            DataInputStream inputStream = new DataInputStream(in);
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, "UTF8"));
            String strLine;
            while ((strLine = br.readLine()) != null) {
                text = strLine;
                text = text.trim().toUpperCase();
                DroneTrajectory trajectory = new DroneTrajectoryImpl(text);
                droneTrajectoryList.add(trajectory);
            }
        } catch (IOException e) {
            log.error("error reading textFile : " + e);
            throw e;
        }
        return droneTrajectoryList;
    }

}
