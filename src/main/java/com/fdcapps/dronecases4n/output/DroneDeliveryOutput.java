/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fdcapps.dronecases4n.output;

import com.fdcapps.dronecases4n.objects.Drone;
import java.io.OutputStream;
import java.util.List;

/**
 * DroneDeliveryOutput is an interface which represents an output to de user to
 * see the drone delivery coordinates
 *
 * @author Felipe Diaz C <felipediazc@fdcapps.com>
 */
public interface DroneDeliveryOutput {

    /**
     * This method is used to show the drone delivery coordinates
     *
     * @param out This is the OutputStream where de coordinates must be printed
     * @param droneTrajectoryList a List with de drone delivery coordinates
     */
    void getDroneDelivery(OutputStream out, List<Drone> droneTrajectoryList);
}
