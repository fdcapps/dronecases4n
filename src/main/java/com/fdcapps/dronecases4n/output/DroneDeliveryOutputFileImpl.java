/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fdcapps.dronecases4n.output;

import com.fdcapps.dronecases4n.objects.Drone;
import com.fdcapps.dronecases4n.objects.Orientation;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;

/**
 * DroneDeliveryOutputFileImpl is the default implementation of the
 * DroneDeliveryOutput interface
 *
 * @author Felipe Diaz C <felipediazc@fdcapps.com>
 */
public class DroneDeliveryOutputFileImpl implements DroneDeliveryOutput {

    /**
     * This method is used to show the drone delivery coordinates
     *
     * @param out This is the OutputStream where de coordinates must be printed
     * @param droneTrajectoryList a List with de drone delivery coordinates
     */
    @Override
    public void getDroneDelivery(OutputStream out, List<Drone> droneTrajectoryList) {
        PrintStream print = new PrintStream(out);
        print.printf("== Reporte de entregas ==\n\n");
        for (int i = 0; i < droneTrajectoryList.size(); i++) {
            Drone d = droneTrajectoryList.get(i);
            print.printf("(%d, %d) dirección %s\n\n", d.getX(), d.getY(), getOrientationEs(d.getOrientation()));
        }
    }

    private String getOrientationEs(Orientation o) {
        switch (o) {
            case EAST:
                return "ORIENTE";
            case NORTH:
                return "NORTE";
            case SOUTH:
                return "SUR";
            case WEST:
                return "OCCIDENTE";
            default:
                break;
        }
        return "";
    }
}
