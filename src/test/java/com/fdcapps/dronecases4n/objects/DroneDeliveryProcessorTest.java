/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fdcapps.dronecases4n.objects;

import static junit.framework.Assert.assertEquals;

/**
 * DroneDeliveryProcessorTest is the test cases for the DroneDeliveryProcessor object
 * implementations
 *
 * @author Felipe Diaz C <felipediazc@fdcapps.com>
 */
public class DroneDeliveryProcessorTest {

    public void testDroneDeliveryProcesor() throws Exception {

        DroneDeliveryProcessorImpl deliverProcessor = new DroneDeliveryProcessorImpl();
        String str = "AAAAIAA";
        DroneTrajectoryImpl trajectory = new DroneTrajectoryImpl(str);

        Drone drone = new DroneImpl(0, 0, Orientation.NORTH);
        drone = deliverProcessor.getDeliveryStatus(drone, trajectory);
        assertEquals(-2, drone.getX().intValue());
        assertEquals(4, drone.getY().intValue());
        assertEquals(Orientation.WEST, drone.getOrientation());
        
        str = "DDDADAI";
        trajectory = new DroneTrajectoryImpl(str);
        drone = deliverProcessor.getDeliveryStatus(drone, trajectory);
        assertEquals(-3, drone.getX().intValue());
        assertEquals(3, drone.getY().intValue());
        assertEquals(Orientation.SOUTH, drone.getOrientation());        

        str = "AADADAD";
        trajectory = new DroneTrajectoryImpl(str);
        drone = deliverProcessor.getDeliveryStatus(drone, trajectory);
        assertEquals(-4, drone.getX().intValue());
        assertEquals(2, drone.getY().intValue());
        assertEquals(Orientation.EAST, drone.getOrientation());          
    }

}
