/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fdcapps.dronecases4n.objects;

import static junit.framework.Assert.assertEquals;

/**
 * DroneTest is the test cases for the Drone
 * object implementations
 *
 * @author Felipe Diaz C <felipediazc@fdcapps.com>
 */
public class DroneTest {

    public void testDroneTrajectory() throws Exception {
        DroneImpl drone = new DroneImpl(1, 2, Orientation.NORTH);
        assertEquals(1, drone.getX().intValue());
        assertEquals(2, drone.getY().intValue());
        assertEquals(Orientation.NORTH, drone.getOrientation());
    }

}
