/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fdcapps.dronecases4n.objects;
import static junit.framework.Assert.*;

/**
 * DroneTrajectoryTest is the test cases for the DroneTrajectory
 * object implementations
 *
 * @author Felipe Diaz C <felipediazc@fdcapps.com>
 */
public class DroneTrajectoryTest {

    public void testDroneTrajectory() throws Exception {

        String str = "AAAIADDI";
        DroneTrajectoryImpl trajectory = new DroneTrajectoryImpl(str);
        assertEquals(str, trajectory.getTrajectory());
    }
}
