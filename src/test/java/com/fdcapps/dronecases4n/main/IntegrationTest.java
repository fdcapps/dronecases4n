/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fdcapps.dronecases4n.main;

import com.fdcapps.dronecases4n.input.DroneDeliveryInputTest;
import com.fdcapps.dronecases4n.objects.Drone;
import com.fdcapps.dronecases4n.objects.DroneDeliveryProcessorImpl;
import com.fdcapps.dronecases4n.objects.DroneTrajectory;
import com.fdcapps.dronecases4n.objects.Orientation;
import com.fdcapps.dronecases4n.output.DroneDeliveryOutputFileImpl;
import java.io.FileOutputStream;
import java.util.List;
import static junit.framework.Assert.assertEquals;

/**
 * These are the integration test cases
 *
 * @author Felipe Diaz C <felipediazc@fdcapps.com>
 */
public class IntegrationTest {

    public void testAll() throws Exception {
        List<DroneTrajectory> list = new DroneDeliveryInputTest().testDroneDeliveryInput();
        DroneDeliveryProcessorImpl deliverProcessor = new DroneDeliveryProcessorImpl();
        List<Drone> droneStatusList = deliverProcessor.getDeliveryStatus(list);

        Drone drone = droneStatusList.get(0);
        assertEquals(-2, drone.getX().intValue());
        assertEquals(4, drone.getY().intValue());
        assertEquals(Orientation.WEST, drone.getOrientation());

        drone = droneStatusList.get(1);
        assertEquals(-3, drone.getX().intValue());
        assertEquals(3, drone.getY().intValue());
        assertEquals(Orientation.SOUTH, drone.getOrientation());

        drone = droneStatusList.get(2);
        assertEquals(-4, drone.getX().intValue());
        assertEquals(2, drone.getY().intValue());
        assertEquals(Orientation.EAST, drone.getOrientation());

        DroneDeliveryOutputFileImpl output = new DroneDeliveryOutputFileImpl();
        FileOutputStream out = new FileOutputStream("textFiles/testcases/4drones/out01.txt");
        output.getDroneDelivery(out, droneStatusList);
        out.close();
    }

}
