/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fdcapps.dronecases4n.input;

import com.fdcapps.dronecases4n.objects.DroneTrajectory;
import java.io.FileInputStream;
import java.util.List;
import static junit.framework.Assert.assertEquals;

/**
 * DroneDeliveryInputTest is the test cases for the DroneDeliveryInput object
 * implementations
 *
 * @author Felipe Diaz C <felipediazc@fdcapps.com>
 */
public class DroneDeliveryInputTest {

    public List<DroneTrajectory> testDroneDeliveryInput() throws Exception {
        FileInputStream in = new FileInputStream("textFiles/testcases/4drones/in01.txt");

        DroneDeliveryInputFileImpl deliveryInputFile = new DroneDeliveryInputFileImpl();
        List<DroneTrajectory> trajectoryList = deliveryInputFile.getDeliveryInput(in);

        DroneTrajectory t1 = trajectoryList.get(0);
        DroneTrajectory t2 = trajectoryList.get(1);
        DroneTrajectory t3 = trajectoryList.get(2);

        assertEquals(t1.getTrajectory(), "AAAAIAA");
        assertEquals(t2.getTrajectory(), "DDDADAI");
        assertEquals(t3.getTrajectory(), "AADADAD");

        return trajectoryList;
    }
}
